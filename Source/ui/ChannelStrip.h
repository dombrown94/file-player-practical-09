//
//  ChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 17/12/2014.
//
//

#ifndef __Assignment2__ChannelStrip__
#define __Assignment2__ChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "LevelMeters.h"



class ChannelStrip :    public Component,
                        public Slider::Listener
{
public:
    /**Constructor*/
    ChannelStrip();
    /**Destructor*/
    ~ChannelStrip();
    /**Changes the sizes of the child components*/
    void resized();
    /**empty function - used by higher components*/
    void sliderValueChanged(Slider* slider);
    
    /**Function to access the channel strip's slider value*/
    Slider* getLevel() {return &level;}
    /**Function to access the channel strip's level meter*/
    LevelMeters* getLevelMeter() {return &levelMeter; }
    
private:
    float gain;
    Slider level;
    LevelMeters levelMeter;
};

#endif /* defined(__Assignment2__ChannelStrip__) */
