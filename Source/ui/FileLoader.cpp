//
//  FileLoader.cpp
//  Assignment2
//
//  Created by Dom Brown on 19/01/2015.
//
//

#include "FileLoader.h"

FileLoader::FileLoader()
{
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
}
FileLoader::~FileLoader()
{
    delete fileChooser;
}
void FileLoader::resized()
{
    fileChooser->setBounds(0,0,getWidth(),getHeight());
}
void FileLoader::filesDropped(const StringArray& files, int x, int y)
{
    File newFile (*files.begin());
    fileChooser->setCurrentFile(newFile, true);
    filenameComponentChanged(fileChooser);
}
void FileLoader::setFilePlayer(FilePlayer* filePlayer)
{
    filePlayerPtr = filePlayer;
}
void FileLoader::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            if (filePlayerPtr != nullptr)
            {
                filePlayerPtr->loadFile(audioFile);
            }
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
bool FileLoader::isInterestedInFileDrag (const StringArray& files)
{
    DBG("Is Interested in File Drag");
    File newFile (*files.begin());
    
    if (newFile.getFileExtension() == formatManager.getWildcardForAllFormats())
    {
        return true;
        DBG("Item being dragged is Audio File");
    }
    else
    {
        return false;
        DBG("Item being dragged is not Audio File");
    }
}