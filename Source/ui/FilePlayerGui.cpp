/*
  ==============================================================================

    FilePlayerGui.cpp
    Created: 22 Jan 2013 2:49:07pm
    Author:  tj3-mitchell

  ==============================================================================
*/

#include "FilePlayerGui.h"

FilePlayerGui::FilePlayerGui() : filePlayer (nullptr)
{
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    AudioFormatManager formatManager;
    formatManager.registerBasicFormats();
    fileChooser = new FilenameComponent ("audiofile",
                                         File::nonexistent,
                                         true, false, false,
                                         formatManager.getWildcardForAllFormats(),
                                         String::empty,
                                         "(choose a WAV or AIFF file)");
    fileChooser->addListener(this);
    addAndMakeVisible(fileChooser);
    
    playbackSlider.setSliderStyle(juce::Slider::SliderStyle::LinearHorizontal);
    playbackSlider.setRange(0.0, 1.0);
    playbackSlider.addListener(this);
    addAndMakeVisible(playbackSlider);
    
}

FilePlayerGui::~FilePlayerGui()
{
    delete fileChooser;
}

void FilePlayerGui::setFilePlayer (FilePlayer* filePlayer_)
{
    filePlayer = filePlayer_;
}

//Component
void FilePlayerGui::resized()
{
    playButton.setBounds (0, 0, getHeight()/2, getHeight()/2);
    fileChooser->setBounds (getHeight()/2, 0, getWidth()-(getHeight()/2), getHeight()/2);
    playbackSlider.setBounds(0, getHeight()/2, getWidth(), getHeight()/2);
    
    
}

//Button Listener
void FilePlayerGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        if (filePlayer != nullptr)
        {
            filePlayer->setPlaying(!filePlayer->isPlaying());
        }
        if (filePlayer->isPlaying() == true) {
            startTimer(250);
        }
        else if (filePlayer->isPlaying() == false) {
            stopTimer();
        }
    }
}
//Slider Listener
void FilePlayerGui::sliderValueChanged (Slider* slider)
{
    if (slider == &playbackSlider) {
        filePlayer->setPosition(slider->getValue());
    }
}

//FilenameComponentListener
void FilePlayerGui::filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged)
{
    if (fileComponentThatHasChanged == fileChooser)
    {
        File audioFile (fileChooser->getCurrentFile().getFullPathName());
        
        if(audioFile.existsAsFile())
        {
            if (filePlayer != nullptr)
            {
                filePlayer->loadFile(audioFile);
            }
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "sdaTransport",
                                         "Couldn't open file!\n\n");
        }
    }
}
void FilePlayerGui::timerCallback()
{
    playbackSlider.setValue (filePlayer->getPosition() / filePlayer->getLengthOfFile());
    
}