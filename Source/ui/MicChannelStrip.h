//
//  MicChannelStrip.h
//  Assignment2
//
//  Created by Dom Brown on 03/01/2015.
//
//

#ifndef __Assignment2__MicChannelStrip__
#define __Assignment2__MicChannelStrip__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "ChannelStrip.h"
#include "Audio.h"

class MicChannelStrip   :   public Component,
                            public Slider::Listener,
                            public Timer,
                            public Button::Listener
{
public:
    /**Constructor*/
    MicChannelStrip();
    /**Destructor*/
    ~MicChannelStrip();
    /**Sets up the pointer to the audio class*/
    void setAudio(Audio* audio_);
    /**Varies the gain of the audio input*/
    void sliderValueChanged(Slider* slider) override;
    /**Sets the sizes of the child components*/
    void resized() override;
    /**Refreshes the level meters at a set time period*/
    void timerCallback() override;
    void buttonClicked(Button* button) override;
    
    void paint(Graphics& g) override;
private:
    ChannelStrip channelStrip;
    Audio* audio;
    TextButton onOffButton;
    
};

#endif /* defined(__Assignment2__MicChannelStrip__) */
