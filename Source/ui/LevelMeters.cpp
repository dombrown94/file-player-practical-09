//
//  LevelMeters.cpp
//  Assignment2
//
//  Created by Dom Brown on 22/12/2014.
//
//

#include "LevelMeters.h"

LevelMeters::LevelMeters()
{

}
LevelMeters::~LevelMeters()
{
    
}
void LevelMeters::paint(Graphics &g)
{

    int peakL = 1.0 * peakLevelL * getHeight();
    if (peakL > getHeight()) {
        peakL = getHeight();
    }
    int peakR = 1.0 * peakLevelR * getHeight();
    if (peakR > getHeight()) {
        peakR = getHeight();
    }
    g.fillAll(juce::Colours::darkgrey);

    g.setColour(juce::Colours::yellow);
    g.fillRect(0, getHeight() - peakL, getWidth()/2, getHeight());
    g.fillRect(getWidth()/2, getHeight() - peakR, getWidth()/2, getHeight());
    //DBG(peakLevelL);
}
void LevelMeters::setPeakLevel(float newPeakLevelL, float newPeakLevelR)
{
    //left
    newPeakLevelL = newPeakLevelL * 9.0 + 1.0;
    newPeakLevelL = log10f(newPeakLevelL);
    //right
    newPeakLevelR = newPeakLevelR * 9.0 + 1.0;
    newPeakLevelR = log10f(newPeakLevelR);
    //left
    if (newPeakLevelL > peakLevelL) {
        peakLevelL = newPeakLevelL;
    }
    else {
        peakLevelL = peakLevelL * 0.9;
    }
    //right
    if (newPeakLevelR > peakLevelR) {
        peakLevelR = newPeakLevelR;
    }
    else {
        peakLevelR = peakLevelR * 0.9;
    }
    repaint();
}
