/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainContentComponent::MainContentComponent (Audio& audio_) : audio (audio_)
{
    int count;
    setSize (800, 600);
    //masChStrip initialisations
    masterChannelStrip.setAudio(&audio);
    addAndMakeVisible(masterChannelStrip);
    
    micChannelStrip.setAudio(&audio);
    addAndMakeVisible(micChannelStrip);
    
    for (int count; count < NUM_FILE_PLAYERS; count++) {
        filePlayerLabel[count].setText("File Player", NotificationType::dontSendNotification);
        filePlayerLabel[count].setColour(Label::ColourIds::backgroundColourId, Colours::lightcyan);
        addAndMakeVisible(filePlayerLabel[count]);
    }
    micLabel.setText("Microphone", NotificationType::dontSendNotification);
    micLabel.setColour(Label::ColourIds::backgroundColourId, Colours::coral);
    addAndMakeVisible(micLabel);
    masterLabel.setText("Master", NotificationType::dontSendNotification);
    masterLabel.setColour(Label::ColourIds::backgroundColourId, Colours::lightblue);
    addAndMakeVisible(masterLabel);
    
    //connects the fileChannelStrips to their respective FilePlayers and FileLoaders
    for (count = 0; count < NUM_FILE_PLAYERS; count++) {
        fileChannelStrip[count].setFilePlayer(audio.getFilePlayer(count));
        fileChannelStrip[count].getFileLoader()->setFilePlayer(audio.getFilePlayer(count));
        addAndMakeVisible(fileChannelStrip[count]);
    }
}
MainContentComponent::~MainContentComponent()
{
}
void MainContentComponent::resized()
{
    //sets up the four file player channel strips
    for (int count = 0; count < NUM_FILE_PLAYERS; count++) {
        int x = count * (getWidth()/6);
        filePlayerLabel[count].setBounds(x, 0, getWidth()/6, 40);
        fileChannelStrip[count].setBounds(x, 40, getWidth()/6, getHeight()-40);
    }
    //master channel strip and mic channel strip set up
    masterLabel.setBounds(getWidth() - getWidth()/6, 0, getWidth()/6, 40);
    masterChannelStrip.setBounds(getWidth() - getWidth()/6, 40, getWidth()/6, getHeight()-40);
    micLabel.setBounds(getWidth() - getWidth()/3, 0, getWidth()/6, 40);
    micChannelStrip.setBounds(getWidth() - getWidth()/3, 40, getWidth()/6, getHeight()-40);
}
void MainContentComponent::paint(Graphics &g)
{
    g.fillAll(Colours::grey);
}
//MenuBarCallbacks
StringArray MainContentComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainContentComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainContentComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}
