//
//  MasterChannelStrip.cpp
//  Assignment2
//
//  Created by Dom Brown on 22/12/2014.
//
//

#include "MasterChannelStrip.h"

MasterChannelStrip::MasterChannelStrip()
{
    //channel strip set up
    channelStrip.getLevel()->addListener(this);
    addAndMakeVisible(channelStrip);
    startTimer(50);
    
    //record button set up
    recordButton.setButtonText("O");
    recordButton.setColour(TextButton::buttonColourId, Colours::steelblue);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible(recordButton);
    recordButton.addListener(this);
    
    //save button set up
    saveButton.setButtonText("Save");
    saveButton.setColour(TextButton::buttonColourId, Colours::steelblue);
    addAndMakeVisible(saveButton);
    saveButton.addListener(this);
    
}
MasterChannelStrip::~MasterChannelStrip()
{
    
}
void MasterChannelStrip::paint(Graphics& g)
{
    g.fillAll(Colours::lightblue);
}
void MasterChannelStrip::sliderValueChanged(Slider* slider)
{
    if (slider == channelStrip.getLevel()) {
        audio->setGain(slider->getValue());
    }
    
}
void MasterChannelStrip::resized()
{
    channelStrip.setBounds(0, 40, getWidth(), getHeight()-40);
    recordButton.setBounds(0, 0, 40, 40);
    saveButton.setBounds(40, 0, getWidth()-40, 40);
}
void MasterChannelStrip::setAudio(Audio* audio_)
{
    audio = audio_;
}
void MasterChannelStrip::timerCallback()
{
    channelStrip.getLevelMeter()->setPeakLevel(audio->getPeak(0), audio->getPeak(1));
    startTimer(50);
}
void MasterChannelStrip::buttonClicked(Button* button)
{
    if (button == &recordButton)
    {
        audio->setRecording(!audio->isRecording());
        if (audio->isRecording() == true) {
            recordButton.setColour(TextButton::buttonColourId, Colours::red);
        }
        else
        {
            recordButton.setColour(TextButton::buttonColourId, Colours::steelblue);
        }
    }
    if (button == &saveButton) {
        audio->getRecorder()->saveToFile();
    }
    
}

