/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "../Audio/Audio.h"
#include "FileChannelStrip.h"
#include "MasterChannelStrip.h"
#include "MicChannelStrip.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainContentComponent   :  public Component,
                                public DragAndDropContainer,
                                public MenuBarModel
{
public:
    //==============================================================================
    /**Constructor*/
    MainContentComponent(Audio& audio_);
    /**Destructor*/
    ~MainContentComponent();
    /**Used to set the sizes of the child components*/
    void resized() override;
    /**Sets the colour*/
    void paint(Graphics &g);
    //MenuBarEnums/Callbacks
    enum Menus
    {
        FileMenu = 0,
        
        NumMenus
    };
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;

private:
    Audio& audio;
    FileChannelStrip fileChannelStrip[NUM_FILE_PLAYERS];
    MasterChannelStrip masterChannelStrip;
    MicChannelStrip micChannelStrip;
    
    Label filePlayerLabel[NUM_FILE_PLAYERS];
    Label micLabel;
    Label masterLabel;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
