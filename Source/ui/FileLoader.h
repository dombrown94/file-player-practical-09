//
//  FileLoader.h
//  Assignment2
//
//  Created by Dom Brown on 19/01/2015.
//
//

#ifndef __Assignment2__FileLoader__
#define __Assignment2__FileLoader__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

class FileLoader :  public FileDragAndDropTarget,
                    public FilenameComponentListener,
                    public Component

{
public:
    /**Constructor*/
    FileLoader();
    /**Destructor*/
    ~FileLoader();
    /**Recieves a file that has been drag and dropped and then updates the file to be played*/
    void filesDropped (const StringArray& files, int x, int y) override;
    /**checks whether the file being dropped is an audio file*/
    bool isInterestedInFileDrag (const StringArray& files) override;
    /**Sets the pointer to the fileplayer*/
    void setFilePlayer(FilePlayer* filePlayer);
    /**Updates the file to be played by the filePlayer*/
    void filenameComponentChanged (FilenameComponent* fileComponentThatHasChanged);
    /**sets the boundaries for components*/
    void resized() override;
    
    //void paint(Graphics& g) override;
private:
    
    FilePlayer* filePlayerPtr;
    FilenameComponent* fileChooser;
    AudioFormatManager formatManager;
};

#endif /* defined(__Assignment2__FileLoader__) */
