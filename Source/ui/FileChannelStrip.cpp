//
//  FileChannelStrip.cpp
//  Assignment2
//
//  Created by Dom Brown on 18/12/2014.
//
//

#include "FileChannelStrip.h"

FileChannelStrip::FileChannelStrip() : filePlayer(nullptr)
{
    //connects the file channel strip to the slider in the channel strip within it
    channelStrip.getLevel()->addListener(this);
    addAndMakeVisible(channelStrip);
    
    playButton.setButtonText (">");
    playButton.addListener(this);
    addAndMakeVisible(&playButton);
    
    addAndMakeVisible(&fileLoader);

    startTimer(50);
    

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FileChannelStrip::~FileChannelStrip()
{

}
void FileChannelStrip::paint(Graphics& g)
{
    g.fillAll(Colours::lightcyan);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileChannelStrip::resized()
{
    playButton.setBounds(0, 0, 40, 40);
    channelStrip.setBounds(0, 40, getWidth(), getHeight()-40);
    fileLoader.setBounds(40, 0, getWidth()-40, 40);
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileChannelStrip::buttonClicked(Button* button)
{
    if (button == &playButton) {
        if (filePlayer != nullptr)
        {
            filePlayer->setPlaying(!filePlayer->isPlaying());
            //swaps the play symbol for a stop symbol
            if (filePlayer->isPlaying() == true)
            {
                playButton.setButtonText("[]");
            }
            else
            {
                playButton.setButtonText(">");
            }
        }
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileChannelStrip::setFilePlayer (FilePlayer* filePlayer_)
{
    filePlayer = filePlayer_;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileChannelStrip::sliderValueChanged (Slider* slider)
{
    if (slider == channelStrip.getLevel()) {
        filePlayer->getAudioTransportSource()->setGain(slider->getValue());
    }
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FileChannelStrip::timerCallback()
{
    startTimer(50);
    channelStrip.getLevelMeter()->setPeakLevel(filePlayer->getRMS(0), filePlayer->getRMS(1));
}

