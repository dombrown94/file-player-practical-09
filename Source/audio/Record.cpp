//
//  Record.cpp
//  Assignment2
//
//  Created by Dom Brown on 20/01/2015.
//
//

#include "Record.h"
#include "../../JuceLibraryCode/JuceHeader.h"

Record::Record() :   thread("SaveThread")
{
    audioSampleBuffer.setSize(2, recordSize);
    audioSampleBuffer.clear();
    
    audioTextFile = fopen("audio.txt", "w");
}
Record::~Record()
{
    thread.stopThread(100);
}
void Record::save()
{
    recordState = false;
    FileChooser chooser ("Please select a file...", File::getSpecialLocation(File::userDesktopDirectory), "*wav");
    if (chooser.browseForFileToSave(true)) {
        File file (chooser.getResult().withFileExtension(".wav"));
        WavAudioFormat wavFormat;
        OutputStream* outStream = file.createOutputStream();
        AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 2, 16, NULL, 0);
        writer->writeFromAudioSampleBuffer(audioSampleBuffer, 0, sampleIndex);
        delete writer;
    }
}
void Record::setRecording(bool newState)
{
    recordState = newState;
    
    if (recordState == true) {
        DBG("recording");
        audioSampleBuffer.clear();
        audioTextFile = fopen("audio.txt", "w");
        sampleIndex = 0;
        recordIndex = 0;
        
    }
    if (recordState == false) {
        DBG("not recording");
        fclose(audioTextFile);
    }
}
void Record::recorder(double sampleL, double sampleR)
{
    if (recordState == true && sampleIndex < recordSize)
    {
        float* audioSampleL = audioSampleBuffer.getWritePointer(0, sampleIndex);
        *audioSampleL = sampleL;
        float* audioSampleR = audioSampleBuffer.getWritePointer(1, sampleIndex);
        *audioSampleR = sampleR;
        sampleIndex++;
    }
    if (sampleIndex == recordSize) {
        sampleIndex = 0;
    }
}
bool Record::isRecording()
{
    return recordState;
}
void Record::recordToFile(float sampleL, float sampleR)
{
    if (recordState == true)
    {
        fprintf(audioTextFile, "%f %f\n", sampleL, sampleR);
        recordIndex++;

    }
}
void Record::saveToFile()
{
    thread.startThread();
    
    recordState = false;
    audioTextFile = fopen("audio.txt", "r");
    
    
    if (audioTextFile != NULL) {
        
        float currentSampleL, currentSampleR;
        
        AudioSampleBuffer audioRecordBuffer;
        audioRecordBuffer.setSize(2, recordIndex);
        audioRecordBuffer.clear();
        DBG(recordIndex);
        
        for (int index = 0; index < recordIndex; index++) {
            fscanf(audioTextFile, "%f %f\n", &currentSampleL, &currentSampleR);
            
            float* audioSampleL = audioRecordBuffer.getWritePointer(0, index);
            *audioSampleL = currentSampleL;
            float* audioSampleR = audioRecordBuffer.getWritePointer(1, index);
            *audioSampleR = currentSampleR;
        }
        
        FileChooser chooser ("Please select a file...", File::getSpecialLocation(File::userDesktopDirectory), "*wav");
        if (chooser.browseForFileToSave(true)) {
            File file (chooser.getResult().withFileExtension(".wav"));
            WavAudioFormat wavFormat;
            OutputStream* outStream = file.createOutputStream();
            AudioFormatWriter* writer = wavFormat.createWriterFor(outStream, 44100, 2, 16, NULL, 0);
            writer->writeFromAudioSampleBuffer(audioRecordBuffer, 0, recordIndex);
            delete writer;
        }
    }
}