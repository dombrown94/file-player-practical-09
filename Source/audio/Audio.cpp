//
//  Audio.cpp
//  Assignment2
//
//  Created by Dom Brown on 15/12/2014.
//
//

#include "Audio.h"

Audio::Audio()//  :   recorder(recorder)
{
    audioDeviceManager.initialiseWithDefaultDevices(2, 2);
    audioDeviceManager.addAudioCallback(this);
    audioDeviceManager.addMidiInputCallback(String::empty, this);

    //mixes the outputs of the fileplayers together
    for (int count = 0 ; count < NUM_FILE_PLAYERS; count++) {
        mixerAudioSource.addInputSource(&filePlayer[count], 0);
    }
    audioSourcePlayer.setSource(&mixerAudioSource);
}
Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
    mixerAudioSource.removeAllInputs();
    
}
void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    audioSourcePlayer.audioDeviceIOCallback(inputChannelData,
                                            numInputChannels,
                                            outputChannelData,
                                            numOutputChannels,
                                            numSamples);
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float inSampL;
    float inSampR;
    
    
    while(numSamples--)
    {
        //file input
        inSampL = *outL;
        inSampR = *outR;
        //mic input
        inValL = *inL * micGain;
        inValR = *inR * micGain;
        
        *outL = (inSampL + inValL) * gain;
        *outR = (inSampR + inValR) * gain;
        
        peak[0] = fabsf(*outL);
        peak[1] = fabsf(*outR);
        
        //record(*outL, *outR);
        //recorder.recorder(*outL, *outR);
        recorder.recordToFile(*outL, *outR);
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}
void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart(device);
}
void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}
void Audio::handleIncomingMidiMessage (MidiInput* source,
                                       const MidiMessage& message)
{
    
}

void Audio::setGain(float inGain)
{
    gain = inGain;
}
void Audio::setRecording(bool newState)
{
    recorder.setRecording(newState);
}
void Audio::save()
{
    recorder.save();
}
void Audio::setMicOnOffState(bool newOnOffState)
{
    micOnOffState = newOnOffState;
}
void Audio::setMicGain(float inGain)
{

    if (micOnOffState == true) {
        micGain = inGain;
    }
    else
        micGain = 0;
    
}
