//
//  Record.h
//  Assignment2
//
//  Created by Dom Brown on 20/01/2015.
//
//

#ifndef __Assignment2__Record__
#define __Assignment2__Record__

#include <stdio.h>
#include "../../JuceLibraryCode/JuceHeader.h"


class Record
{
public:
    /**Constructor*/
    Record();
    /**Destructor*/
    ~Record();
    /**Saves the audio from the audio buffer to a wav file*/
    void save();
    /**Starts and stops the recording process*/
    void setRecording(bool newState);
    /**The samples to be recorded get passed into hear*/
    void recorder(double sampleL, double sampleR);
    /**Returns whether or not the recorder is recording*/
    bool isRecording();
    /**Records the audio sample buffer into a seperate text file*/
    void recordToFile(float sampleL, float sampleR);
    /**Takes the audio data from the file and turns it into a wav file*/
    void saveToFile();
private:
    AudioSampleBuffer audioSampleBuffer;
    bool recordState;
    long sampleIndex;
    long recordSize = 2646000;
    FILE* audioTextFile;
    long recordIndex = 0;
    
    TimeSliceThread thread;
};

#endif /* defined(__Assignment2__Record__) */
