//
//  Audio.h
//  Assignment2
//
//  Created by Dom Brown on 15/12/2014.
//
//

#ifndef __Assignment2__Audio__
#define __Assignment2__Audio__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"
#include "Record.h"

#define NUM_FILE_PLAYERS 4

class Audio :   public AudioIODeviceCallback,
                public MidiInputCallback
{
public:
    /**Constructor*/
    Audio();
    /**Destructor*/
    ~Audio();
    /**Audio Process Function: all audio processing is done here*/
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /**Called when audio device is started*/
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /**Called when audio device stopped*/
    void audioDeviceStopped();
    /**Handles MIDI messages (currently unused)*/
    void handleIncomingMidiMessage (MidiInput* source,
                                    const MidiMessage& message) override;
    
    /**Sets the overall gain*/
    void setGain (float inGain);
    /**sets the recording state */
    void setRecording(bool newState);
    /** saves the audio buffer as an audio file */
    void save();

    
    /**Functions that return variables in Audio that need to be used elsewhere*/
    AudioSourcePlayer* getAudioSourcePlayer() { return &audioSourcePlayer; }
    
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    
    FilePlayer* getFilePlayer(int index) { return &filePlayer[index]; }
    
    MixerAudioSource* getMixerAudioSource() {return &mixerAudioSource; }
    
    bool isRecording() { return recorder.isRecording(); }
    
    Record* getRecorder() {return &recorder;}
    
    /**Returns the current peak of the audio to be used in level meters*/
    float getPeak(int index) { return peak[index]; }
    /** this function returns the gain value of the input*/
    float getMicGain() {return micGain;}
    /** This function sets the gain value for the input */
    void setMicGain(float inGain);
    /**These functions return the audio in values for use in the mic channel strip level meters*/
    float getInValL() {return fabsf(inValL);}
    float getInValR() {return fabsf(inValR);}
    
    void setMicOnOffState(bool newOnOffState);
    bool getMicOnOffState() {return micOnOffState;}
    
    
private:
    AudioDeviceManager audioDeviceManager;
    AudioSourcePlayer audioSourcePlayer;
    FilePlayer filePlayer[NUM_FILE_PLAYERS];
    MixerAudioSource mixerAudioSource;
    
    Record recorder;
    
    int sampleIndex = 0;
    int bufferSizeIndex;
    float gain = 0.0;
    float micGain = 0.0;
    float peak[2];
    
    float inValL;
    float inValR;
    
    bool recordState = false;
    bool micOnOffState = false;
};

#endif /* defined(__Assignment2__Audio__) */
